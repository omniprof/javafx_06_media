package com.kenfogel.javafx_06_media;

import java.net.URISyntaxException;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.stage.Stage;

/**
 * Example of playing a video
 *
 * @author Ken
 */
public class MainApp extends Application {

    /**
     * Start the JavaFX application
     *
     * @param primaryStage
     * @throws java.net.URISyntaxException
     */
    @Override
    public void start(Stage primaryStage) throws URISyntaxException {

        primaryStage.setTitle("San Francisco Street Car");

        // Video is 16 x 9 so I calculated the correct ratio
        int width = 800;
        int height = 450;
        String path = "/video/SFCableCar.MP4";

        Group root = new Group();
        root.getChildren().add(createMediaView(path, width, height));
        primaryStage.setScene(new Scene(root, width, height));
        primaryStage.show();
    }

    /**
     * Create a MediaView that scales the media
     *
     * @param width
     * @param height
     * @return
     * @throws URISyntaxException
     */
    private MediaView createMediaView(String path, int width, int height) throws URISyntaxException {
        Media media = new Media(getClass().getResource(path).toURI().toString());
        MediaPlayer mediaPlayer = new MediaPlayer(media);
        mediaPlayer.setAutoPlay(true);
        MediaView mediaView = new MediaView(mediaPlayer);
        mediaView.setFitHeight(width);
        mediaView.setFitWidth(width);
        return mediaView;
    }

    /**
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}
 